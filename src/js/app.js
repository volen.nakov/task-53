import "../scss/app.scss";

window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready
  let products = document.querySelectorAll('.product')
  for (let i = 0; i < products.length; i++) {
    let price = products[i].querySelector('.price').textContent
    products[i].setAttribute('data-price', price)
  }

});
